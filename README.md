**Scandiweb Junior Developer test**

The task was to create two pages - product add and product list. In the first page user can add one of three items( dvd, book, furniture),
where each of them contains unique SKU, name, price and different parameter depending on the item. The other page displays already added 
items, it also has a mass deletion option, where any amount of items can be checked via checkbox and deleted after pressing 'delete' button.

The code is written in html, css, javascript(additionally using JQuery) and php which is used for creating objects and adding or deleting them
from the database.
